import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {

        int numberOfThreads = setNumberOfThreads();

        ExecutorService myService = Executors.newFixedThreadPool(numberOfThreads);
        System.out.println("Process is using " + numberOfThreads + " concurrent threads.");

        Random random = new Random();

        Set<Player> listOfPlayers = new HashSet<>();

        Player player01 = new Player("Peter", random.nextInt(3));
        listOfPlayers.add(player01);
        LevelMatcher levelMatcher1 = new LevelMatcher(player01, listOfPlayers);

        Player player02 = new Player("John", random.nextInt(3));
        listOfPlayers.add(player02);
        LevelMatcher levelMatcher2 = new LevelMatcher(player02, listOfPlayers);

        Player player03 = new Player("Mary", random.nextInt(3));
        listOfPlayers.add(player03);
        LevelMatcher levelMatcher3 = new LevelMatcher(player03, listOfPlayers);

        Player player04 = new Player("Antony", random.nextInt(3));
        listOfPlayers.add(player04);
        LevelMatcher levelMatcher4 = new LevelMatcher(player04, listOfPlayers);

        Player player05 = new Player("Carl", random.nextInt(3));
        listOfPlayers.add(player05);
        LevelMatcher levelMatcher5 = new LevelMatcher(player05, listOfPlayers);

        Player player06 = new Player("James", random.nextInt(3));
        listOfPlayers.add(player06);
        LevelMatcher levelMatcher6 = new LevelMatcher(player06, listOfPlayers);

        myService.execute(levelMatcher1);
        myService.execute(levelMatcher2);
        myService.execute(levelMatcher3);
        myService.execute(levelMatcher4);
        myService.execute(levelMatcher5);
        myService.execute(levelMatcher6);

        try {
            myService.shutdown();
            myService.awaitTermination(20, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
            System.err.println("Tasks interrupted successfully");
        }
        finally {
            if (!myService.isTerminated()) {
                System.err.println("Cancel non-finished tasks");
            }
            myService.shutdownNow();
            System.out.println("Shutdown process finished successfully");
        }
    }

    // Sets number of threads to be used in the program depending on user's processors
    public static int setNumberOfThreads() {
        int coreCount = Runtime.getRuntime().availableProcessors();
        int maxThreads;
        if (coreCount > 2) {
            maxThreads = coreCount - 2;
        } else {
            maxThreads = coreCount;
        }
        return maxThreads;
    }

    public void createPlayer(String name, Integer level) {
        Player player = new Player(name, level);

        validatePlayerName(player);
        validatePlayerLevel(player);
    }

    private void validatePlayerName(Player player) {
        player.validatePlayerName();
    }

    private void validatePlayerLevel(Player player) {
        player.validatePlayerLevel();
    }
}
