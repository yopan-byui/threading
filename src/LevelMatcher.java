import java.util.Random;
import java.util.Set;

public class LevelMatcher implements Runnable {
    private Player player;
    private Set<Player> listOfPlayers;
    private int sleep;


    public LevelMatcher(Player player, Set<Player> listOfPlayers) {
        this.player = player;
        this.listOfPlayers = listOfPlayers;

        Random random = new Random();
        this.sleep = random.nextInt(2000 - 1000) + 1000;
    }

    @Override
    public void run() {
        System.out.println(player.getName() + ", who is level " + player.getLevel() + " is now waiting for an adversary.");

        for (Player currentPlayer : listOfPlayers) {
            int level = currentPlayer.getLevel();
            String playerName = currentPlayer.getName();

            if(level == this.player.getLevel() && playerName != this.player.getName()) {
                System.out.println(currentPlayer.getName() + " has found other player(s) with the same level to play against.");
            } else {
                try {
                    Thread.sleep(sleep);
                    System.out.println("Waiting for next player...");
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
    }
}