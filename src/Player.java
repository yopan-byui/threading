public class Player {
    private String name;
    private Integer level;

    public Player(String name, Integer level) {
        this.name = name;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String level) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public void validatePlayerName() {
        if(this.name.isBlank()) {
            throw new RuntimeException("Player name can not be null or blanket");
        }
    }

    public void validatePlayerLevel() {
        if(this.level < 0) {
            throw new RuntimeException("Player level can not be negative");
        }
    }
}
